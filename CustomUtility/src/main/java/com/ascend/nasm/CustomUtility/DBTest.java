package com.ascend.nasm.CustomUtility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;


public class DBTest {
	
	private static String getEnv(String dbServer) {
		String businessUnit = "TEST-";
		return dbServer.contains("-") ? businessUnit.concat(dbServer.split("-")[1]) : dbServer;
	}
	
	public static void main(String[] args) throws SQLException {
		String databaseServer = "Test";
		String connectionString = "jdbc:sqlserver://"+databaseServer+";user=manish;password=manish;database=test";
		Connection con = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(connectionString);
			JOptionPane.showMessageDialog(null, "Connection Established!!", getEnv(databaseServer), JOptionPane.INFORMATION_MESSAGE );
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), getEnv(databaseServer), JOptionPane.ERROR_MESSAGE);
		} finally {
			if(null!=con)
			con.close();
		}
	}
}
