package pattern.composite;

@FunctionalInterface
public interface Expression {


    double getValue();
}
