package pattern.composite;

public class TestExpression {
//Calculate (2+3)*5/15-6
    public static void main (String[] args) {
        Expression e= new Divider(new Multiplier(new Adder(new Constant(2),new Constant(3)),new Constant(5)),new Subtracter(new Constant(15),new Constant(5)));

    System.out.println(e.getValue());

    }
}
